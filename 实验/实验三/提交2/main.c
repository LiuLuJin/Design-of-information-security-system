#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>


void * print_a(void *);
void * print_b(void *);

int main(){

    pthread_t t0;
    pthread_t t1;

    // 创建线程A
    if(pthread_create(&t0, NULL, print_a, NULL) == -1){
        puts("fail to create pthread t0");
        exit(1);
    }

    if(pthread_create(&t1, NULL, print_b, NULL) == -1){
        puts("fail to create pthread t1");
        exit(1);
    }

    // 等待线程结束
    void * result;
    if(pthread_join(t0, &result) == -1){
        puts("fail to recollect t0");
        exit(1);
    }

    if(pthread_join(t1, &result) == -1){
        puts("fail to recollect t1");
        exit(1);
    }


    return 0;
}


// 线程A 方法
void * print_a(void *a){
    for(int i = 0;i < 5; i++){
	FILE *fp;
    char ch;
    int flag=0,num=0;
    fp=fopen("test1.txt","r");
    while((ch=fgetc(fp))!=EOF)
    {
        if(ch==' ' || ch=='\n' || ch=='\t' || ch=='\r')
        {
            flag=0;
        }
        else
        {
            if(flag==0)
            {
                flag=1;
                num++;
            }

        }

    }
    printf("test1.txt有%d个单词\n",num);
    fclose(fp);
        sleep(1);
    }
    return NULL;

}

// 线程B 方法
void * print_b(void *b){
    for(int i = 0;i < 10; i++){
        FILE *fp;
    char ch;
    int flag=0,num=0;
    fp=fopen("test2.txt","r");
    while((ch=fgetc(fp))!=EOF)
    {
        if(ch==' ' || ch=='\n' || ch=='\t' || ch=='\r')
        {
            flag=0;
        }
        else
        {
            if(flag==0)
            {
                flag=1;
                num++;
            }

        }

    }
    printf("test2.txt有%d个单词\n",num);
    fclose(fp);
        sleep(1);
    }
    return NULL;

}
